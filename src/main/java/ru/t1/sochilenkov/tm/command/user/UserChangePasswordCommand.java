package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Change password for current user.";

    public static final String NAME = "user-change-password";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
