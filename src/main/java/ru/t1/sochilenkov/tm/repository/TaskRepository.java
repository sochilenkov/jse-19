package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
